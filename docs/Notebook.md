# Notebook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_base64_encoded** | **str** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**extension** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**subscribers** | **list[int]** |  | [optional] 
**user_owner_id** | **int** |  | [optional] 
**version** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


