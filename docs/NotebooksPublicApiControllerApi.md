# bdl_client.NotebooksPublicApiControllerApi

All URIs are relative to *http://192.168.241.1:62230*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_using_post**](NotebooksPublicApiControllerApi.md#create_using_post) | **POST** /api/v1/notebooks | create
[**delete_using_delete**](NotebooksPublicApiControllerApi.md#delete_using_delete) | **DELETE** /api/v1/notebooks/{notebookName} | delete
[**get_all_using_get**](NotebooksPublicApiControllerApi.md#get_all_using_get) | **GET** /api/v1/notebooks | getAll
[**get_using_get**](NotebooksPublicApiControllerApi.md#get_using_get) | **GET** /api/v1/notebooks/{notebookName} | get
[**get_using_head**](NotebooksPublicApiControllerApi.md#get_using_head) | **HEAD** /api/v1/notebooks/{notebookName} | get
[**update_using_put**](NotebooksPublicApiControllerApi.md#update_using_put) | **PUT** /api/v1/notebooks | update


# **create_using_post**
> Notebook create_using_post(user_id, notebook)

create

### Example
```python
from __future__ import print_function
import time
import bdl_client
from bdl_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = bdl_client.NotebooksPublicApiControllerApi()
user_id = 56 # int | UserID
notebook = bdl_client.Notebook() # Notebook | notebook

try:
    # create
    api_response = api_instance.create_using_post(user_id, notebook)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotebooksPublicApiControllerApi->create_using_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| UserID | 
 **notebook** | [**Notebook**](Notebook.md)| notebook | 

### Return type

[**Notebook**](Notebook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_using_delete**
> delete_using_delete(user_id, notebook_name)

delete

### Example
```python
from __future__ import print_function
import time
import bdl_client
from bdl_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = bdl_client.NotebooksPublicApiControllerApi()
user_id = 56 # int | UserID
notebook_name = 'notebook_name_example' # str | notebookName

try:
    # delete
    api_instance.delete_using_delete(user_id, notebook_name)
except ApiException as e:
    print("Exception when calling NotebooksPublicApiControllerApi->delete_using_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| UserID | 
 **notebook_name** | **str**| notebookName | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_using_get**
> list[Notebook] get_all_using_get()

getAll

### Example
```python
from __future__ import print_function
import time
import bdl_client
from bdl_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = bdl_client.NotebooksPublicApiControllerApi()

try:
    # getAll
    api_response = api_instance.get_all_using_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotebooksPublicApiControllerApi->get_all_using_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Notebook]**](Notebook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_using_get**
> Notebook get_using_get(user_id, notebook_name, subscriber=subscriber)

get

### Example
```python
from __future__ import print_function
import time
import bdl_client
from bdl_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = bdl_client.NotebooksPublicApiControllerApi()
user_id = 56 # int | UserID
notebook_name = 'notebook_name_example' # str | notebookName
subscriber = 56 # int | Subscriber (optional)

try:
    # get
    api_response = api_instance.get_using_get(user_id, notebook_name, subscriber=subscriber)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotebooksPublicApiControllerApi->get_using_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| UserID | 
 **notebook_name** | **str**| notebookName | 
 **subscriber** | **int**| Subscriber | [optional] 

### Return type

[**Notebook**](Notebook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_using_head**
> Notebook get_using_head(user_id, notebook_name, subscriber=subscriber)

get

### Example
```python
from __future__ import print_function
import time
import bdl_client
from bdl_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = bdl_client.NotebooksPublicApiControllerApi()
user_id = 56 # int | UserID
notebook_name = 'notebook_name_example' # str | notebookName
subscriber = 56 # int | Subscriber (optional)

try:
    # get
    api_response = api_instance.get_using_head(user_id, notebook_name, subscriber=subscriber)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotebooksPublicApiControllerApi->get_using_head: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| UserID | 
 **notebook_name** | **str**| notebookName | 
 **subscriber** | **int**| Subscriber | [optional] 

### Return type

[**Notebook**](Notebook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_using_put**
> Notebook update_using_put(user_id, notebook)

update

### Example
```python
from __future__ import print_function
import time
import bdl_client
from bdl_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = bdl_client.NotebooksPublicApiControllerApi()
user_id = 56 # int | UserID
notebook = bdl_client.Notebook() # Notebook | notebook

try:
    # update
    api_response = api_instance.update_using_put(user_id, notebook)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotebooksPublicApiControllerApi->update_using_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| UserID | 
 **notebook** | [**Notebook**](Notebook.md)| notebook | 

### Return type

[**Notebook**](Notebook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

